package store;

import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class BookstoreCalculatorTest {

    BookstoreCalculator store = new BookstoreCalculator();

    @Test
    void basicPrice() {
        int[] noBooks = {};
        int[] oneUniqueBook1 = {0};
        int[] oneUniqueBook2 = {1};
        int[] oneUniqueBook3 = {2};
        int[] oneUniqueBook4 = {3};
        int[] multipleDuplicateBooks = {1,1,1};
        assertEquals(0,store.price(noBooks));
        assertEquals(8,store.price(oneUniqueBook1));
        assertEquals(8,store.price(oneUniqueBook2));
        assertEquals(8,store.price(oneUniqueBook3));
        assertEquals(8,store.price(oneUniqueBook4));
        assertEquals(8*3,store.price(multipleDuplicateBooks));
    }

    @Test
    void duplicates(){
        int[] twoUniqueBooks = {0,1};
        int[] threeUniqueBooks = {0,2,3};
        int[] fourUniqueBooks = {0,1,2,3};
        assertEquals(8 * 2 * 0.95, store.price(twoUniqueBooks));
        assertEquals(8 * 3 * 0.9, store.price(threeUniqueBooks));
        assertEquals(8 * 4 * 0.8, store.price(fourUniqueBooks));
    }

    @Test
    void multipleDiscounts(){
        int[] duplicatesAndUniques = {0,0,1};
        int[] dupes2 = {0,0,1,1};
        int[] dupes3 = {0, 0, 1, 2, 2, 3};
        assertEquals(8 + (8 * 2 * 0.95), store.price(duplicatesAndUniques));
        assertEquals(2 * (8 * 2 * 0.95), store.price(dupes2));
        assertEquals((8 * 4 * 0.8) + (8 * 2 * 0.95), store.price(dupes3));

    }
}