package store;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashSet;
import java.util.List;
import java.util.stream.Collectors;

public class BookstoreCalculator {
    public double price(int[] books){
        double[] discounts = {1,1,0.95,0.9,0.8};
        // Convert to List -> using stream, can foreach through books and do the same.
        ArrayList<Integer> originalBookList = (ArrayList<Integer>) Arrays.stream(books).boxed().collect(Collectors.toList());

        /*// We make a subset of all the unique books -> Using stream
        List<Integer> uniqueBooksList = originalBookList.stream().distinct().collect(Collectors.toList());
        // Not using stream
        HashSet<Integer> uniqueBooksList2 = new HashSet<>(originalBookList);*/

        /*// Count the unique ones
        int uniqueBooks = uniqueBooksList.size();*/
        List<List<Integer>> listofUniques = new ArrayList<>();
        extractUniques(originalBookList, listofUniques);
        double total = 0;

        for (List<Integer> bookList: listofUniques) {
            total += 8*bookList.size()*discounts[bookList.size()];
        }

        return total;
    }

    private void extractUniques(ArrayList<Integer> books, List<List<Integer>> uniqueBooksList){
        HashSet<Integer> uniques = new HashSet<>(books);
        if(uniques.size() == books.size()){
            // All books are unique
            uniqueBooksList.add(books);
        } else {
            // Remove the uniques
            for (Integer unique: uniques) {
                books.remove(unique);
            }
            uniqueBooksList.add(new ArrayList<>(uniques));
            extractUniques(books,uniqueBooksList);
        }
    }



    private int countUniqueBooks(int[] books) {
        int uniqueBooks = 0;
        for (int i = 0; i < books.length; i++) {
            boolean isUnique = true;
            for(int j = i+1; j < books.length; j++){
                if(books[i] == books[j]){
                    isUnique = false;
                    break;
                }
            }
            if(isUnique){
                uniqueBooks++;
            }
        }
        return uniqueBooks;
    }
}

    /*int[] ints = {1, 2, 3};
    List<Integer> intList = new ArrayList<Integer>(ints.length);
        for (int i : ints)
        {
        intList.add(i);
        }*/

    /*public double price(int[] books){
        int uniqueBooks = countUniqueBooks(books);


        if(uniqueBooks == 1){
            return 8*books.length;
        } else if (uniqueBooks == 4){
            return 8*books.length*0.8;
        } else {
            return 8* books.length*(1 - ((uniqueBooks-1)*0.05));
        }
    }

    private int countUniqueBooks(int[] books) {
        int uniqueBooks = 0;
        for (int i = 0; i < books.length; i++) {
            boolean isUnique = true;
            for(int j = i+1; j < books.length; j++){
                if(books[i] == books[j]){
                    isUnique = false;
                    break;
                }
            }
            if(isUnique){
                uniqueBooks++;
            }
        }
        return uniqueBooks;
    }*/
